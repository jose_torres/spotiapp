import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor( private http: HttpClient) {
    console.log('test de service');
   }

   getQuery( query: string){
      const url = `https://api.spotify.com/v1/${ query }`;
      const headers = new HttpHeaders({
        'Authorization': 'Bearer  BQDBiQhAkxXSJj4-13KRX891K7KKLZnLbrtCKc5sl6fZ4c28JKXA7_aI4j_wSQeEZ_cHpLNqbNtIA3isTWM'
      });
      return this.http.get(url, { headers });
   }

   getNewReleases() {
      return this.getQuery('browse/new-releases')
                      .pipe( map( data => {
                        return data['albums'].items;
                      }));
   }

   getArtistas(termino: string){
      return this.getQuery(`search?q=${ termino }&type=artist`)
                      .pipe( map( data => {  return data['artists'].items; }));
   }

   getArtista(id: string){
    return this.getQuery(`artists/${ id }`);
                    // .pipe( map( data => {  return data['artists'].items; }));
   }

   getTopTracks( id: string ){
    return this.getQuery(`artists/${ id }/top-tracks?country=us`)
                     .pipe( map( data => {  return data['tracks']; }));
   }
}

// curl -X "POST" -H "Content-Type: application/x-www-form-urlencoded" -d "grant_type=client_credentials&client_id=d6b55734aa3f4b49af89ae8023b6c69e&client_secret=4659ca881ead4eed81c45197f071db34" https://accounts.spotify.com/api/token